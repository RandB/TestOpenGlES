package com.pajx.testopengles.word

import android.content.Context
import kotlin.math.cos
import kotlin.math.sin

/**
 *User: RandBII
 *Date: 2020/8/17
 *Description:
 */
abstract class RenderAble(context: Context) {

    abstract fun draw(matrix: FloatArray)

    fun mSin(m: Float): Float = sin(Math.toRadians(m.toDouble()).toFloat())

    fun mCos(m: Float): Float = cos(Math.toRadians(m.toDouble()).toFloat())

}