package com.pajx.testopengles.word

/**
 *User: RandBII
 *Date: 2020/8/18
 *Description:
 */
object Cons {
    const val UNIT_SIZE = 1.5F
    const val DIMENSION_2 = 2 // 维度2
    const val DIMENSION_3 = 3 // 维度3
    const val DIMENSION_4 = 4  // 维度4

    val VERTEX_COORDINATOR = floatArrayOf( //坐标轴
        0.0f, 0.0f, 0.0f,  //Z轴
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 0.0f,  //X轴
        1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 0.0f,  //Y轴
        0.0f, 1.0f, 0.0f
    )

    val COLOR_COORDINATOR = floatArrayOf( //坐标轴颜色
        0.0f, 0.0f, 1.0f, 1.0f,  //Z轴:蓝色
        0.0f, 0.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 0.0f, 1.0f,  //X轴：黄色
        1.0f, 1.0f, 0.0f, 1.0f,
        0.0f, 1.0f, 0.0f, 1.0f,  //Y轴：绿色
        0.0f, 1.0f, 0.0f, 1.0f
    )
}