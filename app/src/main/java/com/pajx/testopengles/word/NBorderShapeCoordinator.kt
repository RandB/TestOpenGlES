package com.pajx.testopengles.word

import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.sin

/**
 *User: RandBII
 *Date: 2020/8/19
 *Description: 完美
 */
class NBorderShapeCoordinator(m: Int, radius: Float) {

    val n = m
    val radius = radius
    val vertexCoors = generateVertex()
    val colorCoors = generateColorCoors()

    private fun generateVertex(): FloatArray {

        // 1.先假定是 n= 6 边型  radius = 1 ， 那么分为 6 个扇区 每个扇区 360/6 = 72度

        val vertexCount = n + 2 // 顶点个数
//        val radius = 1 // 半径
        val theta = 360 / n
        val result = FloatArray(vertexCount * 3)

        // 圆心做标
        result[0] = 0f
        result[1] = 0f
        result[2] = 0f

        for (index in 1 until vertexCount) {
            result[3 * index + 0] = radius * cos(PI * index * theta / 180).toFloat()
            result[3 * index + 1] = radius * sin(PI * index * theta / 180).toFloat()
            result[3 * index + 2] = 0f
        }

        return result
    }


    private fun generateColorCoors(): FloatArray {
        val verticeCount = n + 2
        //顶点颜色
        //橙色：0.972549f,0.5019608f,0.09411765f,1.0f
        val colors = FloatArray(verticeCount * 4)
        colors[0] = 1f
        colors[1] = 1f
        colors[2] = 1f
        colors[3] = 1f

        for (i in 1 until verticeCount) {
            colors[4 * i] = 0.972549f
            colors[4 * i + 1] = 0.5019608f
            colors[4 * i + 2] = 0.09411765f
            colors[4 * i + 3] = 1.0f
        }

        return colors
    }

}