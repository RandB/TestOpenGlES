package com.pajx.testopengles.word

import android.content.Context
import android.opengl.GLSurfaceView
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent

/**
 *User: RandBII
 *Date: 2020/8/17
 *Description:
 */
class World(context: Context, atts: AttributeSet? = null) : GLSurfaceView(context) {

    private var mRender: WorldRender? = null
    private val mContext = context

    init {
        setEGLContextClientVersion(2)
        mRender = WorldRender(mContext)
        setRenderer(mRender!!)
        renderMode = RENDERMODE_CONTINUOUSLY
    }



}