package com.pajx.testopengles.word

import android.content.Context
import android.opengl.GLES20
import android.opengl.GLSurfaceView
import com.pajx.testopengles.util.MatrixStack
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

/**
 *User: RandBII
 *Date: 2020/8/17
 *Description:
 */
class WorldRender(context: Context) : GLSurfaceView.Renderer {

    private val TAG = "WorldRender-->"
    private var mContext = context


    private var worldShape: RenderAble? = null


    override fun onSurfaceCreated(gl: GL10?, config: EGLConfig?) {
        GLES20.glClearColor(0.0f, 0f, 0f, 1f)
        worldShape = WorldShape(mContext)
    }

    override fun onSurfaceChanged(gl: GL10?, width: Int, height: Int) {
        GLES20.glViewport(0, 0, width, height)
        val ratio = width / height.toFloat()

        //截锥
        MatrixStack.frustumM(-ratio, ratio, -1f, 1f, 3f, 15f)

        // 相机位置
        MatrixStack.lookAt(3f, 5f, -6f,
            0f, 0f, 0f, 0f, 1f, 0f)
        MatrixStack.reset()
    }


    override fun onDrawFrame(gl: GL10?) {

        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT or GLES20.GL_DEPTH_BUFFER_BIT)

//        MatrixStack.save()
        MatrixStack.rotate(1f, 0f, 1f, 0f)
        worldShape?.draw(MatrixStack.peek())
//        MatrixStack.back()

//        MatrixStack.save()
//        MatrixStack.scale(2f, 2f, 2f)
//        worldShape?.draw(MatrixStack.peek())
//        MatrixStack.back()
//
//        MatrixStack.save()
//        MatrixStack.translate(1f, 1f, 1f)
//        worldShape?.draw(MatrixStack.peek())
//        MatrixStack.back()

        GLES20.glEnable(GLES20.GL_DEPTH_BUFFER_BIT)

    }

}