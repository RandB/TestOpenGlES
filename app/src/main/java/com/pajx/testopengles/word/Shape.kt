package com.pajx.testopengles.word

/**
 *User: RandBII
 *Date: 2020/8/18
 *Description:
 */
  class Shape(vertex: FloatArray, color: FloatArray, type: Int) : Cloneable {

    var mVertex = vertex
    var mColors = color
    val mType = type

    fun moveAndCreate(x: Float, y: Float, z: Float): Shape {
        val s: Shape = clone()
        s.move(x, y, z)
        return s
    }


    fun move(offx: Float, offy: Float, offz: Float) {
        for (index in mVertex.indices) {
            when (index % 3) {
                0 -> mVertex[index] += offx
                1 -> mVertex[index] += offy
                2 -> mVertex[index] += offz

            }
        }
    }

    override fun clone(): Shape {
        var clo: Shape? = null
        clo = super.clone() as Shape
        val v = FloatArray(mVertex.size)
        val c = FloatArray(mColors.size)
        System.arraycopy(mVertex, 0, v, 0, mVertex.size)
        System.arraycopy(mColors, 0, c, 0, mColors.size)
        clo.mVertex = v
        clo.mColors = c
        return clo
    }


}