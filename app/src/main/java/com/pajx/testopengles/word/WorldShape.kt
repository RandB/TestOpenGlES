package com.pajx.testopengles.word

import android.content.Context
import android.opengl.GLES20
import com.pajx.testopengles.views.three_d.ManBallShape
import com.pajx.testopengles.views.three_d.TrianglePyramid
import com.pajx.testopengles.views.three_d.TrianglePyramidBorder

/**
 *User: RandBII
 *Date: 2020/8/18
 *Description:
 */
class WorldShape(context: Context) : RenderAble(context), Op<RenderAble> {


    private val shapes = mutableListOf<RenderAble>()


    private val coors = floatArrayOf(
        -1.0f, 0.0f, -1.0f,
        -1.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 1.0f,
        1.0f, 0.0f, -1.0f
    )
    private val colors = floatArrayOf(
        1.0f, 1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f,
        0.21960784f, 0.56078434f, 0.92156863f, 1.0f
    )

    private val mVertex2 = floatArrayOf(
        1.0f, 1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,

        -1.0f, 1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f,

        -1.0f, 1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,

        1.0f, 1.0f, -1.0f,
        1.0f, -1.0f, -1.0f
    )
    private val mColor2 = floatArrayOf(
        1.0f, 0.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 0.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f
    )


    init {
        val center = Shape(coors, colors, GLES20.GL_LINE_LOOP)

        val top = center.moveAndCreate(0f, 1f, 0f)
        val bottom = center.moveAndCreate(0f, -1f, 0f)
        val coordinate = Shape(Cons.VERTEX_COORDINATOR, Cons.COLOR_COORDINATOR, GLES20.GL_LINES)

        val centerSimpleShape = SimpleShape(context, Shape(coors, colors, GLES20.GL_LINE_LOOP))
        val topSimpleShape = SimpleShape(context, top)
        val bottomSimpleShape = SimpleShape(context, bottom)
        val coordinateSimpleShape = SimpleShape(context, coordinate)

        val fourLineShape = SimpleShape(context, Shape(mVertex2, mColor2, GLES20.GL_LINES))
        val circleShape = SimpleShape(
            context,
            Shape(
                CircleShapeCoordinators.vertexCoors,
                CircleShapeCoordinators.colorCoors,
                GLES20.GL_TRIANGLE_FAN
            )
        )

        // 球体
//        val ball = BallShape(context)
//         漫射光球体
        val manBall = ManBallShape(context)
//        shapes.add(centerSimpleShape)

        val trianglePyramid = TrianglePyramid(context, bian = 3)
        val borderPyramid = TrianglePyramidBorder(context, bian = 3)
        shapes.add(topSimpleShape)
        shapes.add(bottomSimpleShape)
        shapes.add(coordinateSimpleShape)
        shapes.add(fourLineShape)
//        shapes.add(circleShape)
//        shapes.add(ball)
//        shapes.add(manBall)
        shapes.add(trianglePyramid)
//        shapes.add(borderPyramid)

    }

    override fun draw(matrix: FloatArray) {

        for (item in shapes) {
            item.draw(matrix)
        }

    }

    override fun add(vararg t: RenderAble) {
        for (i in t) {
            shapes.add(i)
        }
    }

    override fun rmeove(id: Int) {
        if (id >= 0 && id < shapes.size) shapes.removeAt(id)
    }
}