package com.pajx.testopengles

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.pajx.testopengles.word.World

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        val  view = MyGLSurfaceView(this)
        val view = World(this)
        setContentView(view)
    }
}