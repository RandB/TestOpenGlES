package com.pajx.testopengles.util

import java.nio.FloatBuffer

/**
 *User: RandBII
 *Date: 2020/8/20
 *Description:  直接设置 漫射光向量，获取 缓存值
 *
 * 漫射光.相机.环境光
 */
object GLCameraLightHelper {

    private val lightLocation = floatArrayOf(0f, 0f, 0f)
    var lightPositionFB: FloatBuffer? = null

    fun changeLightLocation(x: Float, y: Float, z: Float) {
        lightLocation[0] = x
        lightLocation[1] = y
        lightLocation[2] = z
        lightPositionFB = GLHelper.getFloatBuffer(lightLocation)
    }

    private val cameraLocation = floatArrayOf(0f, 0f, 0f)
    var cameraPositionFB: FloatBuffer? = null

    fun changeCameraLocation(x: Float, y: Float, z: Float) {
        cameraLocation[0] = x
        cameraLocation[1] = y
        cameraLocation[2] = z
        cameraPositionFB = GLHelper.getFloatBuffer(cameraLocation)
    }

    private val environLight = floatArrayOf(0f, 0f, 0f)
    var environPositionFB: FloatBuffer? = null
    fun changeEnvironLight(x: Float, y: Float, z: Float) {
        environLight[0] = x
        environLight[1] = y
        environLight[2] = z
        environPositionFB = GLHelper.getFloatBuffer(environLight)
    }

}