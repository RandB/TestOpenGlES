package com.pajx.testopengles.views.two_d

import android.content.Context
import android.opengl.GLES20
import com.pajx.testopengles.R
import com.pajx.testopengles.util.GLHelper
import java.nio.FloatBuffer
import java.nio.ShortBuffer

/**
 *User: RandBII
 *Date: 2020/8/13
 *Description:
 *
 * (三角形 添加坐标 和 添加绘制索引  -->变成正方形)
 *
 */

class TextureTriangleView(context: Context) {

    private var mContext = context

    private val coors = floatArrayOf(
        -0.5f, 0.5f, 0f,
        -0.5f, -0.5f, 0f,
        0.5f, -0.5f, 0f,
        0.5f, 0.5f, 0f
    )
    private val textureCoors = floatArrayOf(
        0.0f, 0.0f,
        1.0f, 0.0f,
        0.0f, 1.0f,
        1.0f,1.0f
    )

    private val indexs = shortArrayOf(
        1,2,3,
        0,1,3
    )
    private val COORS_PRE_VERTEX = 3 // 3个坐标代表一个顶点
    private val TEXTURE_PRE_VERTEX = 2 // 2个坐标代表一个顶点
    private var mCoordsBuffer: FloatBuffer? = null
    private var mIndexBuffer: ShortBuffer? = null
    private var mTextureBuffer: FloatBuffer? = null
    private var mProgram = -1
    private var mPositionHandle = -1
    private var mColorHandle = -1
    private var muMVPMatrixHandle = -1
    private val vertexStride = COORS_PRE_VERTEX * 4
    private val vertexTextureStride = TEXTURE_PRE_VERTEX * 4
    private val shaderCode = GLHelper.loadShaderAsserts(context, "rect_texture_vert.glsl")
    private val fragmentShaderCode = GLHelper.loadShaderAsserts(context, "rect_texture_frag.glsl")
    private val textureId = GLHelper.loadTexture(context, resId = R.drawable.aa)

    init {
        mCoordsBuffer = GLHelper.getFloatBuffer(coors)
        mTextureBuffer = GLHelper.getFloatBuffer(textureCoors)
        mIndexBuffer = GLHelper.getShortBuffer(indexs)

        val shaderId = GLHelper.loadShader(GLES20.GL_VERTEX_SHADER, shaderCode)
        val fragmentId = GLHelper.loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode)

        mPositionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition")
        mColorHandle = GLES20.glGetAttribLocation(mProgram, "vCoordinate")
        muMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix")

        mProgram = GLES20.glCreateProgram()

        GLES20.glAttachShader(mProgram, shaderId)
        GLES20.glAttachShader(mProgram, fragmentId)
        GLES20.glLinkProgram(mProgram)
        GLHelper.validateProgram(mProgram)


    }

    fun draw(mvpMatrix: FloatArray) {
        GLES20.glUseProgram(mProgram)
        GLES20.glEnableVertexAttribArray(mPositionHandle)
        GLES20.glEnableVertexAttribArray(mColorHandle)
        GLES20.glUniformMatrix4fv(muMVPMatrixHandle, 1, false, mvpMatrix, 0)

        GLES20.glVertexAttribPointer(
            mPositionHandle,
            COORS_PRE_VERTEX,
            GLES20.GL_FLOAT,
            false,
            vertexStride,
            mCoordsBuffer
        )
        GLES20.glVertexAttribPointer(
            mColorHandle,
            TEXTURE_PRE_VERTEX,
            GLES20.GL_FLOAT,
            false,
            vertexTextureStride,
            mTextureBuffer
        )

        // 绑定纹理
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0)
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureId)

        GLES20.glDrawElements(
            GLES20.GL_TRIANGLES,
            indexs.size,
            GLES20.GL_UNSIGNED_SHORT,
            mIndexBuffer
        )
        GLES20.glDisableVertexAttribArray(mPositionHandle)
    }

}