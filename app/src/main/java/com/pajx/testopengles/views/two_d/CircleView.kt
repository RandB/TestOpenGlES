package com.pajx.testopengles.views.two_d

import android.content.Context
import android.opengl.GLES20
import android.util.Log
import com.pajx.testopengles.util.GLHelper
import java.nio.FloatBuffer
import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.sin

/**
 *User: RandBII
 *Date: 2020/8/13
 *Description:
 *
 * 1.思路  https://blog.csdn.net/xiaozhude/article/details/84763453
 *
 * //  存在问题
 *
 */

class CircleView(context: Context) {
    private val mContext = context
    private val coors = getCircleVertex(4)

    private val colors = floatArrayOf(
        // RGBA
        0.0f, 0.0f, 0.1f, 1.0f
    )

    private var mVertexBuffer: FloatBuffer? = null
    private var mColorBuffer: FloatBuffer? = null
    private var mPositionHandle = -1
    private var mColorHandle = -1
    private var muMVPMatrixHandle = -1
    private var mProgram = -1

    private val COOR_PRE_VERTEX = 3
    private val COLOR_RRE_VERTEX = 4
    val colorStride = COLOR_RRE_VERTEX * 4
    private val vertexStride = COOR_PRE_VERTEX * 4
    private val vertexShader = GLHelper.loadShaderAsserts(mContext, "tri_vert2.glsl")
    private val fragmentShader = GLHelper.loadShaderAsserts(mContext, "tri_frag2.glsl")


    // n 边型
    fun getCircleVertex(n: Int): FloatArray {
        val angle = 360 / n   // 一次旋转的角度
        val result = FloatArray(364 * 3)
        val radius = 0.5f

        // 圆心做标
        result[0] = 0f
        result[1] = 0f
        result[2] = 0f

        for (index in 1..363) {
//            val currentAngle = angle * index
//            Log.e("-->", "currentAngle --> $i")
            result[3 * index + 0] = radius * cos(PI * index / 180).toFloat()
            result[3 * index + 1] = radius * sin(PI * index / 180).toFloat()
            result[3 * index + 2] = 0f
        }


        for (i in result.indices) {
//            Log.e("-->", "${i}--:${result[i]}  ")
        }
        return result

    }

    init {
        Log.e("-->","coors-->${coors.size}")
        mVertexBuffer = GLHelper.getFloatBuffer(coors)

        mColorBuffer = GLHelper.getFloatBuffer(colors)

        val shaderId = GLHelper.loadShader(GLES20.GL_VERTEX_SHADER, vertexShader)
        val fragmentId = GLHelper.loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShader)

        mPositionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition")
        mColorHandle = GLES20.glGetAttribLocation(mProgram, "aColor")
        muMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix")

        mProgram = GLES20.glCreateProgram()
        GLES20.glAttachShader(mProgram, shaderId)
        GLES20.glAttachShader(mProgram, fragmentId)
        Log.e("-->", "mPositionHandle-->${mPositionHandle}-->${mColorHandle}")
        Log.e("-->", "shaderId-->${shaderId}-->${fragmentId}")
        Log.e("-->", "mProgram-->${mProgram}")
        GLES20.glLinkProgram(mProgram)
        GLHelper.validateProgram(mProgram)

    }


    fun draw(mvpMatrix: FloatArray) {

        GLES20.glUseProgram(mProgram)
        GLES20.glEnableVertexAttribArray(mPositionHandle)
        GLES20.glEnableVertexAttribArray(mColorHandle)
        GLES20.glUniformMatrix4fv(muMVPMatrixHandle, 1, false, mvpMatrix, 0)

        GLES20.glVertexAttribPointer(
            mPositionHandle,
            COOR_PRE_VERTEX,
            GLES20.GL_FLOAT,
            false,
            vertexStride,
            mVertexBuffer
        )

        GLES20.glVertexAttribPointer(
            mColorHandle,
            COLOR_RRE_VERTEX,
            GLES20.GL_FLOAT,
            false,
            colorStride,
            mColorBuffer
        )


        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_FAN, 0, coors.size / COOR_PRE_VERTEX)
        GLES20.glDisable(mPositionHandle)


    }


}