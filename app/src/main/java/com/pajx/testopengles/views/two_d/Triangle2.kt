package com.pajx.testopengles.views.two_d

import android.content.Context
import android.opengl.GLES20
import android.util.Log
import com.pajx.testopengles.util.ShaderHelper
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer

/**
 *User: RandBII
 *Date: 2020/8/11
 *Description: 顶点带有颜色
 */

class Triangle2 constructor(context: Context) {

    private var mContext: Context? = null

    init {
        mContext = context
    }

    var vertexShaderCode =
        ShaderHelper.loadShaderAsserts(mContext!!, "tri_vert2.glsl")

    val fragmentShaderCode =
        ShaderHelper.loadShaderAsserts(mContext!!, "tri_frag2.glsl")

    var mProgram: Int? = null
    var mPositionHandle: Int = -1 // 位置句柄
    var mColorHandle: Int = -1  // 颜色句柄
    var muMVPMatrixHandle = 0//顶点变换矩阵句柄
    var vertexBuffer: FloatBuffer? = null // 顶点坐标缓冲
    var mColorBuffer: FloatBuffer? = null // 颜色缓冲
    val COLOR_PRE_VERTEX = 4 // 颜色向量维度
    val COOR_PRE_VERTEX = 3 // 坐标向量
    val vertexColorStride = COLOR_PRE_VERTEX * 4  // 跨度

    val colors = floatArrayOf(
        1f, 1f, 0.0f, 1.0f,//黄
        0.05882353f, 0.09411765f, 0.9372549f, 1.0f,//蓝
        0.19607843f, 1.0f, 0.02745098f, 1.0f//绿
    )

    val coors = floatArrayOf(
        0.0f, 0.0f, 0.0f,// (x,y,z)
        -1f, -1f, 0.0f,
        1f, -1f, 0.0f
    )

    val vertexCount = coors.size / COOR_PRE_VERTEX
    val vertexStride = COOR_PRE_VERTEX * 4

    init {
        // 坐标数乘以 4 字节
        val bb: ByteBuffer =
            ByteBuffer.allocateDirect(coors.size * 4).order(ByteOrder.nativeOrder())
        vertexBuffer = bb.asFloatBuffer()
        vertexBuffer?.apply {
            put(coors)
            position(0)
        }

        val cbb = ByteBuffer.allocateDirect(colors.size * 4).order(ByteOrder.nativeOrder())
        mColorBuffer = cbb.asFloatBuffer().apply {
            put(colors)
            position(0)
        }


        val vertexShader = ShaderHelper.loadShader(GLES20.GL_VERTEX_SHADER, vertexShaderCode)
        Log.e("-->", "vertexShader-->$vertexShader")

        val fragmentShader = ShaderHelper.loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode)
        Log.e("-->", "fragmentShader-->$fragmentShader")
        mProgram = GLES20.glCreateProgram()
        Log.e("-->", "mProgram-->$mProgram")
        GLES20.glAttachShader(mProgram!!, vertexShader)
        GLES20.glAttachShader(mProgram!!, fragmentShader)
        GLES20.glLinkProgram(mProgram!!)
        ShaderHelper.validateProgram(mProgram!!)

    }

    fun draw(mvpMatrix: FloatArray) {
        // 程序添加到GLES20的环境中
        GLES20.glUseProgram(mProgram!!)
        // 获取顶点着色器的vPosition成员的句柄
        mPositionHandle = GLES20.glGetAttribLocation(mProgram!!, "vPosition")
        // 获取顶点着色器 aColor成员的句柄
        mColorHandle = GLES20.glGetAttribLocation(mProgram!!, "aColor")
        // 总变换矩阵uMVPMatrix 成员句柄
        muMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram!!, "uMVPMatrix")


        // 启用三角形顶点坐标的句柄
        GLES20.glEnableVertexAttribArray(mPositionHandle)
//        启用三角形顶点颜色句柄
        GLES20.glEnableVertexAttribArray(mColorHandle)
        // 顶点矩阵变换
        GLES20.glUniformMatrix4fv(muMVPMatrixHandle, 1, false, mvpMatrix, 0)

        //三角形坐标数据
        GLES20.glVertexAttribPointer(
            mPositionHandle,//索引
            COOR_PRE_VERTEX,// 大小
            GLES20.GL_FLOAT, //类型
            false,// 是否标准化
            vertexStride,//跨度
            vertexBuffer //缓冲
        )

//        三角形顶点颜色数据
        GLES20.glVertexAttribPointer(
            mColorHandle,
            COLOR_PRE_VERTEX,
            GLES20.GL_FLOAT,
            false,
            vertexColorStride,
            mColorBuffer
        )

        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertexCount)
        GLES20.glDisableVertexAttribArray(mPositionHandle)
    }


}