package com.pajx.testopengles.views.two_d

import android.content.Context
import android.opengl.GLES20
import com.pajx.testopengles.util.GLHelper
import com.pajx.testopengles.util.ShaderHelper
import java.nio.FloatBuffer
import java.nio.ShortBuffer

/**
 *User: RandBII
 *Date: 2020/8/11
 *Description: 正方形（或者五边形等）
 *
 * （修改坐标和 顶点索引数组）
 */

class RectangleView constructor(context: Context) {

    private var mContext = context
    private val coors = floatArrayOf(
        -0.5f, 0.5f, 0.0f, // 左上
        -0.5f, -0.5f, 0.0f, // 左下
        0.5f, -0.5f, 0.0f, // 右下
        0.5f, 0.5f, 0.0f,//右上
        0.0f, 0.8f, 0.0f // 五边形的顶点

    )

    private val ids = shortArrayOf(
        0, 1, 2,
        0, 2, 3,
        0, 3, 4
    )


    private val colors = floatArrayOf(
        1f, 1f, 0.0f, 1.0f,//黄
        0.05882353f, 0.09411765f, 0.9372549f, 1.0f,//蓝
        0.19607843f, 1.0f, 0.02745098f, 1.0f,//绿
        1.0f, 0.0f, 1.0f, 1.0f//紫色
    )

    private var mVertexBuffer: FloatBuffer? = null
    private var mColorBuffer: FloatBuffer? = null
    private var mIdsBuffer: ShortBuffer? = null
    private val COOR_PRE_VERTEX = 3 // 坐标向量
    private val COLOR_PRE_VERTEX = 4 // 颜色向量
    private var mProgram: Int? = -1
    private var vertexStride = COOR_PRE_VERTEX * 4 //坐标的跨度
    private var vertexSize = coors.size / COOR_PRE_VERTEX
    private var vertexColorStride = COLOR_PRE_VERTEX * 4 // 颜色跨度
    private var mPositionHandle: Int = -1 // 坐标位置句柄
    private var mColorHandle: Int = -1// 颜色句柄
    private var uMVPMatrixHandle: Int = -1 // 变换矩阵 句柄
    private var vertexShaderCode =
        ShaderHelper.loadShaderAsserts(context, "tri_vert2.glsl")
    private var vertexFragmentCode =
        ShaderHelper.loadShaderAsserts(context, "tri_frag2.glsl")


    init {
        mVertexBuffer = GLHelper.getFloatBuffer(coors)
        mColorBuffer = GLHelper.getFloatBuffer(colors)
        mIdsBuffer = GLHelper.getShortBuffer(ids)

        val vertexShaderId = ShaderHelper.loadShader(GLES20.GL_VERTEX_SHADER, vertexShaderCode)
        val fragmentShaderId =
            ShaderHelper.loadShader(GLES20.GL_FRAGMENT_SHADER, vertexFragmentCode)

        mProgram = GLES20.glCreateProgram()
        mProgram?.let {
            GLES20.glAttachShader(it, vertexShaderId)
            GLES20.glAttachShader(it, fragmentShaderId)
            GLES20.glLinkProgram(it)
            ShaderHelper.validateProgram(it)
        }
    }

    fun draw(mvpMatrix: FloatArray) {
        //程序添加到GLES 环境当中
        mProgram?.let {
            GLES20.glUseProgram(it)
            mPositionHandle = GLES20.glGetAttribLocation(it, "vPosition")
            mColorHandle = GLES20.glGetAttribLocation(it, "aColor")
            uMVPMatrixHandle = GLES20.glGetUniformLocation(it, "uMVPMatrix")
        }
        GLES20.glEnableVertexAttribArray(mPositionHandle)
        GLES20.glEnableVertexAttribArray(mColorHandle)
        GLES20.glUniformMatrix4fv(uMVPMatrixHandle, 1, false, mvpMatrix, 0)
        //坐标数据
        GLES20.glVertexAttribPointer(
            mPositionHandle,
            COOR_PRE_VERTEX,
            GLES20.GL_FLOAT,
            false,
            vertexStride,
            mVertexBuffer
        )
        // 颜色数据
        GLES20.glVertexAttribPointer(
            mColorHandle,
            COLOR_PRE_VERTEX,
            GLES20.GL_FLOAT,
            false,
            vertexColorStride,
            mColorBuffer
        )
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, ids.size, GLES20.GL_UNSIGNED_SHORT, mIdsBuffer)
        GLES20.glDisableVertexAttribArray(mPositionHandle)

    }


}