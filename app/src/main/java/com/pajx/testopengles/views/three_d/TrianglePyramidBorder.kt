package com.pajx.testopengles.views.three_d

import android.content.Context
import android.opengl.GLES20
import android.util.Log
import com.pajx.testopengles.util.GLHelper
import com.pajx.testopengles.word.Cons
import com.pajx.testopengles.word.RenderAble
import java.nio.FloatBuffer
import kotlin.math.cos
import kotlin.math.sin


/**
 *User: RandBII
 *Date: 2020/8/21
 *Description: 三棱锥 或者多棱锥侧边
 *
 *TODO  感觉少绘制一个三角形
 *
 */

class TrianglePyramidBorder(context: Context, r: Float = 1.0f, h: Float = 1.0f, bian: Int = 3) :
    RenderAble(context) {
    val n = bian
    val TAG = "TrianglePyramidBorder"
    private val mContext = context
    private val R = r
    private val H = h


    private val vertexes = generateVertex()

    private fun generateVertex(): FloatArray {

        val theta = 360 / n
        val result = FloatArray((n + 2) * 3)
        val count = n+2
        result[0] = 0f
        result[1] = 0f
        result[2] = H


        for (index in 1 until count) {
            result[index * 3 + 0] =
                R * cos(Math.toRadians(theta * (index).toDouble())).toFloat()

            result[index * 3 + 1] =
                R * sin(Math.toRadians(theta * (index).toDouble())).toFloat()

            result[index * 3 + 2] = 0f

        }

        return result

    }


    private val colors = generateColorCoors()


    private val VERTEX_STRIDE = Cons.DIMENSION_3 * 4
    private val COLOR_STRIDE = Cons.DIMENSION_4 * 4

    private var mVertexBuffer: FloatBuffer? = null
    private var mColorBuffer: FloatBuffer? = null
    private var mProgram = -1
    private var mPositionHandle = -1
    private var mColorHandle = -1
    private var mMVPMatrixHandle = -1
    private val shaderCode = GLHelper.loadShaderAsserts(mContext, "TrianglePyramid_vert_b.glsl")
    private val fragmentCode = GLHelper.loadShaderAsserts(mContext, "TrianglePyramid_frag_b.glsl")

    init {
        mVertexBuffer = GLHelper.getFloatBuffer(vertexes)
        mColorBuffer = GLHelper.getFloatBuffer(colors)

        val shaderId = GLHelper.loadShader(GLES20.GL_VERTEX_SHADER, shaderCode)
        val fragmentId = GLHelper.loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentCode)

        mProgram = GLES20.glCreateProgram()
        GLES20.glAttachShader(mProgram, shaderId)
        GLES20.glAttachShader(mProgram, fragmentId)
        GLES20.glLinkProgram(mProgram)
        GLES20.glValidateProgram(mProgram)

        //  Log.e(TAG, "mProgram-->${mProgram}--shadeId-->${shaderId}--fragmentId-->${fragmentId}")


        mPositionHandle = GLES20.glGetAttribLocation(mProgram, "aPosition")
        mColorHandle = GLES20.glGetAttribLocation(mProgram, "aColor")
        mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix")
        Log.e(TAG, "mPositionHandle-->${mPositionHandle}--mColorHandle-->${mColorHandle}")
        Log.e(TAG, "mMVPMatrixHandle-->${mMVPMatrixHandle}")

        for (item in vertexes) {
            Log.e("-->", "-->${item}")
        }
        Log.e("-->", "size-->${vertexes.size}")
    }

    override fun draw(matrix: FloatArray) {

        GLES20.glUseProgram(mProgram)

        GLES20.glEnableVertexAttribArray(mPositionHandle)
        GLES20.glEnableVertexAttribArray(mColorHandle)
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, matrix, 0)

        GLES20.glVertexAttribPointer(
            mPositionHandle,
            Cons.DIMENSION_3,
            GLES20.GL_FLOAT,
            false,
            VERTEX_STRIDE,
            mVertexBuffer
        )
        GLES20.glVertexAttribPointer(
            mColorHandle,
            Cons.DIMENSION_4,
            GLES20.GL_FLOAT,
            false,
            COLOR_STRIDE,
            mColorBuffer
        )


//        Log.e(TAG, "--start--draw--")
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_FAN, 0, vertexes.size / Cons.DIMENSION_3)


    }

    private fun generateColorCoors(): FloatArray {
        val verticeCount = n + 2
        //顶点颜色
        //橙色：0.972549f,0.5019608f,0.09411765f,1.0f
        val colors = FloatArray(verticeCount * 4)
        colors[0] = 1f
        colors[1] = 1f
        colors[2] = 1f
        colors[3] = 1f

        for (i in 0 until verticeCount - 1) {
            colors[4 * i] = 0.972549f
            colors[4 * i + 1] = 0.5019608f
            colors[4 * i + 2] = 0.09411765f
            colors[4 * i + 3] = 1.0f
        }

        return colors
    }
}