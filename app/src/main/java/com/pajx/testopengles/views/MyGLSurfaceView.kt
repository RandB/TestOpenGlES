package com.pajx.testopengles.views

import android.content.Context
import android.opengl.GLSurfaceView
import android.util.AttributeSet
import android.util.Log

/**
 *User: RandBII
 *Date: 2020/8/10
 *Description:
 */

class MyGLSurfaceView(context: Context, attrs: AttributeSet? = null) :
    GLSurfaceView(context, attrs) {

    private var mRender: MyRender? = null

    init {
        initSome()
    }

    private fun initSome() {
        setEGLContextClientVersion(2)
        mRender = MyRender(context)
        setRenderer(mRender)
    }


}