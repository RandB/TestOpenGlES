package com.pajx.testopengles.views

import android.content.Context
import android.opengl.GLES20
import android.opengl.GLSurfaceView
import android.opengl.Matrix
import com.pajx.testopengles.views.three_d.CubicView
import com.pajx.testopengles.views.two_d.CircleView
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

/**
 *User: RandBII
 *Date: 2020/8/10
 *Description:
 */
class MyRender constructor(context: Context) : GLSurfaceView.Renderer {

    var degree = 0f
    val mMVPMatrix = FloatArray(16)

    //投影矩阵
    val mProjectionMatrix = FloatArray(16)

    //视图矩阵
    val mViewMatrix = FloatArray(16)

    var ratio = 1f

    // 对矩阵的一系列变换
    val mOptionsMatrix = FloatArray(16)


    private var mContext: Context? = null

    init {
        mContext = context
    }

    //    private var mTriangle: Triangle2? = null  // 第一个demo
//    private var mTangle: RectangleView? = null  // 第二个demo
//    private var mCubic: CubicView? = null  // 第三个demo

    //    private var mTextureTriangleView: TextureTriangleView? = null  // 第四个demo
    private var mCircleView: CircleView? = null  // 第四个demo
//    private var mCircle: Circle? = null  // 第5个demo

    override fun onSurfaceCreated(gl: GL10?, config: EGLConfig?) {
        GLES20.glClearColor(1.0f, 0.0f, 0.0f, 0.0f)
//        mTriangle = Triangle2(mContext!!)
//        mTangle = RectangleView(mContext!!)
//        mCubic = CubicView(mContext!!)
//        mTextureTriangleView = TextureTriangleView(mContext!!)
        mCircleView = CircleView(mContext!!)
//        mCircle = Circle(mContext!!)
    }

    override fun onSurfaceChanged(gl: GL10?, width: Int, height: Int) {
        GLES20.glViewport(0, 0, width, height)
        ratio = width.toFloat() / height.toFloat()

        // 透视投影矩阵 （截锥）
        /**(矫正图形比例)
         * mViewMatrix 获取结果
         */
        Matrix.frustumM(
            mProjectionMatrix, 0,
            -ratio, ratio, -1f, 1f,
            3f, 12f
        )

//        设置相机位置
        Matrix.setLookAtM(
            mViewMatrix, 0,
            0f, 2f, -4f,
            0f, 0f, 0f,
            0f, 1f, 0f
        )

        Matrix.multiplyMM(
            mMVPMatrix, 0,
            mProjectionMatrix, 0,
            mViewMatrix, 0
        )

    }

    override fun onDrawFrame(gl: GL10?) {

        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT or GLES20.GL_DEPTH_BUFFER_BIT)


//        Matrix.setRotateM(
//            mOptionsMatrix, 0,
//            degree,// 角度ff
//            0f, 1f, 0f// x, y z 轴
//        )
//        Matrix.translateM(mOptionsMatrix,0,0f,degree/5f,0f)

        Matrix.multiplyMM(
            mMVPMatrix, 0,
            mViewMatrix, 0,
            mOptionsMatrix, 0
        )
        Matrix.multiplyMM(
            mMVPMatrix, 0,
            mProjectionMatrix, 0,
            mMVPMatrix, 0
        )

//        Log.e("--", "degree-->${degree}")
//        mTriangle?.draw(mMVPMatrix)

//        mTextureTriangleView?.draw(mMVPMatrix)

//        mCircle?.draw(mMVPMatrix)
        mCircleView?.draw(mMVPMatrix)
//        mCubic?.draw(mMVPMatrix)

//        degree++
//        if (degree == 360f) {
//            degree = 0f
//        }

        GLES20.glEnable(GLES20.GL_DEPTH_TEST)
    }


}