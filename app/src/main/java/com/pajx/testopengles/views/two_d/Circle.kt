package com.pajx.testopengles.views.two_d

import android.content.Context
import android.opengl.GLES20
import android.util.Log
import com.pajx.testopengles.util.GLHelper
import java.nio.FloatBuffer
import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.sin

/**
 * 网上搜的
 */
class Circle internal constructor(context: Context) {
    private val mContext: Context = context
    var mColorBuffer: FloatBuffer? = null
    private val mProgram: Int
    private var mPositionHandle = 0
    private var mColorHandle = 0
    private var mMVPMatrixHandle = 0
    private var mVertexBuffer: FloatBuffer? = null
    private val vertices = getCircleVertex(1)
    var color = floatArrayOf(
        0f, 0.0f, 0.56f, 1.0f
    )

    private val vertexShaderCode = GLHelper.loadShaderAsserts(context, "tri_vert2.glsl")
    private val fragmentShaderCode = GLHelper.loadShaderAsserts(context, "tri_frag2.glsl")
//    private val vertexShaderCode = "uniform mat4 uMVPMatrix;" +
//            "attribute vec4 vPosition;" +
//            "void main() {" +
//            "  gl_Position = uMVPMatrix * vPosition;" +
//            "}"
//    private val fragmentShaderCode = "precision mediump float;" +
//            "uniform vec4 vColor;" +
//            "void main() {" +
//            "  gl_FragColor = vColor;" +
//            "}"

    init {
//        vertices[0] = 0F
//        vertices[1] = 0F
//        vertices[2] = 0F
//        for (i in 1..363) {
//            vertices[i * 3 + 0] =
//                (0.5 * Math.cos(3.14 / 180 * i.toFloat())).toFloat()
//            vertices[i * 3 + 1] =
//                (0.5 * Math.sin(3.14 / 180 * i.toFloat())).toFloat()
//            vertices[i * 3 + 2] = 0F
//        }

        mVertexBuffer =
            GLHelper.getFloatBuffer(vertices)

        mColorBuffer = GLHelper.getFloatBuffer(color)
        val vertexShader = loadShader(
            GLES20.GL_VERTEX_SHADER,
            vertexShaderCode
        )
        val fragmentShader = loadShader(
            GLES20.GL_FRAGMENT_SHADER,
            fragmentShaderCode
        )
        mProgram = GLES20.glCreateProgram() // create empty OpenGL ES Program
        GLES20.glAttachShader(mProgram, vertexShader) // add the vertex shader to program
        GLES20.glAttachShader(mProgram, fragmentShader) // add the fragment shader to program
        GLES20.glLinkProgram(mProgram)
    }

    fun draw(mvpMatrix: FloatArray?) {
        GLES20.glUseProgram(mProgram)

        // get handle to vertex shader's vPosition member
        mPositionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition")

        // Enable a handle to the triangle vertices
        GLES20.glEnableVertexAttribArray(mPositionHandle)

        // Prepare the triangle coordinate data
        GLES20.glVertexAttribPointer(
            mPositionHandle, 3,
            GLES20.GL_FLOAT, false, 12
            , mVertexBuffer
        )

        // get handle to fragment shader's vColor member
        mColorHandle = GLES20.glGetAttribLocation(mProgram, "aColor")
        GLES20.glEnableVertexAttribArray(mColorHandle)

        GLES20.glVertexAttribPointer(
            mColorHandle, 4,
            GLES20.GL_FLOAT, false, 16
            , mColorBuffer
        )

        // Set color for drawing the triangle
//        GLES20.glUniform4fv(mColorHandle, 1, color, 0)
        mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix")

        // Apply the projection and view transformation
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mvpMatrix, 0)


        // Draw the triangle
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_FAN, 0, 364)

        // Disable vertex array
        GLES20.glDisableVertexAttribArray(mPositionHandle)
    }

    companion object {
        fun loadShader(type: Int, shaderCode: String?): Int {
            val shader = GLES20.glCreateShader(type)
            GLES20.glShaderSource(shader, shaderCode)
            GLES20.glCompileShader(shader)
            return shader
        }
    }

    // n 边型
    fun getCircleVertex(n: Int): FloatArray {
        val angle = 360 / n   // 一次旋转的角度
        val result = FloatArray(364 * 3)
        val radius = 0.5f

        // 圆心做标
        result[0] = 0f
        result[1] = 0f
        result[2] = 0f

        for (index in 1..363) {
//            val currentAngle = angle * index
//            Log.e("-->", "currentAngle --> $i")
            result[3 * index + 0] = radius * cos(PI * index / 180).toFloat()
            result[3 * index + 1] = radius * sin(PI * index / 180).toFloat()
            result[3 * index + 2] = 0f
        }


        for (i in result.indices) {
            Log.e("-->", "${i}--:${result[i]}  ")
        }
        return result

    }


}