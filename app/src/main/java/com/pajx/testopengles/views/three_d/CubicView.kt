package com.pajx.testopengles.views.three_d

import android.content.Context
import android.opengl.GLES20
import android.util.Log
import com.pajx.testopengles.util.GLHelper
import com.pajx.testopengles.util.ShaderHelper
import java.nio.FloatBuffer
import java.nio.ShortBuffer

/**
 *User: RandBII
 *Date: 2020/8/12
 *Description:
 *
 */

class CubicView(context: Context) {

    private val mContext = context


    val coors = floatArrayOf(
        //A面
        -0.5f, 0.5f, 0.5f,//p0
        -0.5f, -0.5f, 0.5f,//p1
        -0.5f, -0.5f, -0.5f,//p2
        -0.5f, 0.5f, -0.5f,//p3
        //B面
        -0.5f, 0.5f, 0.5f,//p4
        -0.5f, -0.5f, 0.5f,//p5
        0.5f, -0.5f, 0.5f,//p6
        0.5f, 0.5f, 0.5f,//p7
        //C面
        0.5f, 0.5f, 0.5f,//p8
        0.5f, -0.5f, 0.5f,//p9
        0.5f, -0.5f, -0.5f,//p10
        0.5f, 0.5f, -0.5f,//p11
        //D面
        -0.5f, 0.5f, 0.5f,//p12
        0.5f, 0.5f, 0.5f,//p13
        0.5f, 0.5f, -0.5f,//p14
        -0.5f, 0.5f, -0.5f,//p15
        //E面
        -0.5f, -0.5f, 0.5f,//p16
        0.5f, -0.5f, 0.5f,//p17
        0.5f, -0.5f, -0.5f,//p18
        -0.5f, -0.5f, -0.5f,//p19
        //F面
        -0.5f, 0.5f, -0.5f,//p20
        -0.5f, -0.5f, -0.5f,//p21
        0.5f, -0.5f, -0.5f,//p22
        0.5f, 0.5f, -0.5f//p23
    )


    private val idxs = shortArrayOf(
        0, 1, 3,//A
        1, 2, 3,
        0 + 4, 1 + 4, 3 + 4,//B
        1 + 4, 2 + 4, 3 + 4,
        0 + 4 * 2, 1 + 4 * 2, 3 + 4 * 2,//C
        1 + 4 * 2, 2 + 4 * 2, 3 + 4 * 2,
        0 + 4 * 3, 1 + 4 * 3, 3 + 4 * 3,//D
        1 + 4 * 3, 2 + 4 * 3, 3 + 4 * 3,
        0 + 4 * 4, 1 + 4 * 4, 3 + 4 * 4,//E
        1 + 4 * 4, 2 + 4 * 4, 3 + 4 * 4,
        0 + 4 * 5, 1 + 4 * 5, 3 + 4 * 5,//F
        1 + 4 * 5, 2 + 4 * 5, 3 + 4 * 5
    )


    val colors = floatArrayOf(
        //A
        1f, 1f, 0.0f, 1.0f,//黄
        0.05882353f, 0.09411765f, 0.9372549f, 1.0f,//蓝
        0.19607843f, 1.0f, 0.02745098f, 1.0f,//绿
        1.0f, 0.0f, 1.0f, 1.0f,//紫色
        //B
        1f, 1f, 0.0f, 1.0f,//黄
        0.05882353f, 0.09411765f, 0.9372549f, 1.0f,//蓝
        0.19607843f, 1.0f, 0.02745098f, 1.0f,//绿
        1.0f, 0.0f, 1.0f, 1.0f,//紫色
        //C
        1f, 1f, 0.0f, 1.0f,//黄
        0.05882353f, 0.09411765f, 0.9372549f, 1.0f,//蓝
        0.19607843f, 1.0f, 0.02745098f, 1.0f,//绿
        1.0f, 0.0f, 1.0f, 1.0f,//紫色
        //D
        1f, 1f, 0.0f, 1.0f,//黄
        0.05882353f, 0.09411765f, 0.9372549f, 1.0f,//蓝
        0.19607843f, 1.0f, 0.02745098f, 1.0f,//绿
        1.0f, 0.0f, 1.0f, 1.0f,//紫色
        //E
        1f, 1f, 0.0f, 1.0f,//黄
        0.05882353f, 0.09411765f, 0.9372549f, 1.0f,//蓝
        0.19607843f, 1.0f, 0.02745098f, 1.0f,//绿
        1.0f, 0.0f, 1.0f, 1.0f,//紫色
        //F
        1f, 1f, 0.0f, 1.0f,//黄
        0.05882353f, 0.09411765f, 0.9372549f, 1.0f,//蓝
        0.19607843f, 1.0f, 0.02745098f, 1.0f,//绿
        1.0f, 0.0f, 1.0f, 1.0f//紫色
    )


    private var mVertexBuffer: FloatBuffer? = null
    private var mColorBuffer: FloatBuffer? = null
    private var mIdsBuffer: ShortBuffer? = null
    private val COOR_PRE_VERTEX = 3
    private val COLOR_PRE_VERTEX = 4


    private var vertexShaderCode =
        ShaderHelper.loadShaderAsserts(mContext, "tri_vert2.glsl")
    private var vertexFragmentCode =
        ShaderHelper.loadShaderAsserts(mContext, "tri_frag2.glsl")
    private var mProgram = -1
    private var mPositionHandle = -1
    private var uMVPMatrixHandle: Int = -1 // 变换矩阵 句柄
    private var mColorHandle = -1
    private val vertexStride = COOR_PRE_VERTEX * 4
    private val colorStride = COLOR_PRE_VERTEX * 4

    init {
        mVertexBuffer = GLHelper.getFloatBuffer(coors)
        mColorBuffer = GLHelper.getFloatBuffer(colors)
        mIdsBuffer = GLHelper.getShortBuffer(idxs)

        val shaderId = ShaderHelper.loadShader(GLES20.GL_VERTEX_SHADER, vertexShaderCode)
        val fragmentId = ShaderHelper.loadShader(GLES20.GL_FRAGMENT_SHADER, vertexFragmentCode)
        Log.e("-->", "shaderId--:${shaderId}  fragmentId-->${fragmentId}")

        mProgram = GLES20.glCreateProgram()

        GLES20.glAttachShader(mProgram, shaderId)
        GLES20.glAttachShader(mProgram, fragmentId)
        GLES20.glLinkProgram(mProgram)


        ShaderHelper.validateProgram(mProgram)
        Log.e("-->", "mProgram--:${mProgram}")
        Log.e("-->", "----------------->")

    }

    fun draw(mvpMatrix: FloatArray) {

        GLES20.glUseProgram(mProgram)
        mPositionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition")
        mColorHandle = GLES20.glGetAttribLocation(mProgram, "aColor")
        uMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix")

        GLES20.glEnableVertexAttribArray(mPositionHandle)
        GLES20.glEnableVertexAttribArray(mColorHandle)
        GLES20.glUniformMatrix4fv(uMVPMatrixHandle, 1, false, mvpMatrix, 0)

        //坐标数据缓冲
        GLES20.glVertexAttribPointer(
            mPositionHandle,
            COOR_PRE_VERTEX,
            GLES20.GL_FLOAT,
            false,
            vertexStride,
            mVertexBuffer
        )
        // 颜色数据缓冲
        GLES20.glVertexAttribPointer(
            mColorHandle,
            COLOR_PRE_VERTEX,
            GLES20.GL_FLOAT,
            false,
            colorStride,
            mColorBuffer
        )

        GLES20.glDrawElements(GLES20.GL_TRIANGLES, idxs.size, GLES20.GL_UNSIGNED_SHORT, mIdsBuffer)
        GLES20.glDisableVertexAttribArray(mPositionHandle)

    }

}