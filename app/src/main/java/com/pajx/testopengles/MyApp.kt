package com.pajx.testopengles

import android.app.Application

/**
 *User: RandBII
 *Date: 2020/8/11
 *Description:
 */
class MyApp : Application() {

    companion object {
        var instance: MyApp = MyApp()
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

    }

}